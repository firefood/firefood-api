'use strict';
const {
  Model
} = require('sequelize');
const geoip = require('geoip-lite')

module.exports = (sequelize, DataTypes) => {
  class Token extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {

    }
  };
  Token.init({
    userId: DataTypes.INTEGER,
    token: DataTypes.STRING,
    expiresIn: DataTypes.DATE,
    userAgent: DataTypes.STRING,
    ip: DataTypes.STRING,
    location: DataTypes.STRING,
    deleted: {
      type: DataTypes.BOOLEAN,
      default: false
    }
  }, {
    sequelize,
    modelName: 'Token',
  });

  Token.generate = async (userId, req) => {
    let token = ''
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    let charactersLength = characters.length

    for (let i = 0; i < 64; i++) {
      token += characters.charAt(Math.floor(Math.random() * charactersLength))
    }

    const expiresIn = new Date()
    expiresIn.setDate(expiresIn.getDate() + 30)

    const geo = geoip.lookup(req.ip)

    return Token.create({
      userId: userId,
      token: token,
      userAgent: req.headers['user-agent'],
      ip: req.ip,
      location: `${(geo ? geo.country: 'Неизвестно')}, ${(geo ? geo.region: 'Неизвестно')}`,
      expiresIn: expiresIn
    })
  }

  return Token;
};