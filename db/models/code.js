'use strict';
const {
  Model, Op
} = require('sequelize');
const moment = require('moment')

module.exports = (sequelize, DataTypes) => {
  class Code extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Code.init({
    phone: DataTypes.BIGINT,
    code: DataTypes.NUMBER,
    used: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Code',
  });

  Code.generate = async phone => {
    let generated_code = 0

    while (generated_code === 0) {
      let temp = Math.floor(Math.random() * (9999 - 1000) + 1000)

      let codeDatabase = await Code.findOne({ where: { code: temp, used: false, createdAt: {
        [Op.gt]: moment().subtract(10, 'minutes')
          } } })

      if(!codeDatabase) generated_code = temp
    }

    return generated_code
  }

  return Code;
};