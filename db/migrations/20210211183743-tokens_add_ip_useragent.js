'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'Tokens',
        'userAgent',
        {
          type: Sequelize.STRING,
        }
      ),
      queryInterface.addColumn(
        'Tokens',
        'ip',
        {
          type: Sequelize.STRING,
        }
      ),
      queryInterface.addColumn(
        'Tokens',
        'location',
        {
          type: Sequelize.STRING,
        }
      )
    ])
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('Tokens', 'userAgent'),
      queryInterface.removeColumn('Tokens', 'ip'),
      queryInterface.removeColumn('Tokens', 'location'),
    ])
  }
};
