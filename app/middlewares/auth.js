const { Op } = require('sequelize')
const moment = require('moment')
const { Token, User } = require('../../db/models')

const auth = async (req, res, next) => {
  try {
    const token = req.query.access_token
    const tokenModel = await Token.findOne({ where: { token: token, expiresIn: {
          [Op.gte]: moment(),
        }, deleted: null } })
    if (!tokenModel) {
      throw new Error()
    }

    const user = await User.findOne({ where: { id: tokenModel.userId } })
    if (!user) {
      throw new Error()
    }
    req.user = user
    req.token = tokenModel
    next()
  } catch (error) {
    res.status(401).send({
      status: 'error',
      message: 'wrong-token'
    })
  }
}

module.exports = auth
