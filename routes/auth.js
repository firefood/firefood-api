const { Router } = require('express')
const { checkSchema, validationResult } = require('express-validator')
const { Op } = require('sequelize')
const moment = require('moment')
const axios = require('axios')
const { User, Token, Code } = require('../db/models')
const authMiddleware = require('../app/middlewares/auth')

const router = Router()

router.post('/login', checkSchema({
  phone: {
    exists: {
      errorMessage: 'Не указан номер телефона'
    },
    matches: {
      options: /^77[0-9]{9}$/,
      errorMessage: 'Укажите реальный номер телефона'
    },
  }
}), async (req, res) => {
  const errors = validationResult(req)
  if(!errors.isEmpty()) return res.status(422).json({
    type: 'error',
    errors: errors.array()
  })

  const generated_code = await Code.generate(req.body.phone)

  const apiKey = process.env.API_KEY
  const text = `FireFood Kokshe\nКод для входа: ${generated_code}\nInsta: http://mbzn.co/0qn7`

  const apiUrl = new URL('https://api.mobizon.kz/service/message/SendSmsMessage/')
  apiUrl.searchParams.append('apiKey', apiKey)
  apiUrl.searchParams.append('recipient', req.body.phone)
  apiUrl.searchParams.append('text', text)

  axios.post(apiUrl.href).then(async response => {
    const data = response.data

    await Code.create({
      phone: req.body.phone,
      code: generated_code,
      used: false
    })

    return res.status(200).send({
      status: 'ok',
      message: 'Код отправлен.'
    })
  }).catch(err => {
    console.log(err)

    return res.status(422).send({
      status: 'ok',
      message: 'Произошла какая-то ошибка. Попробуйте позже.'
    })
  })
})

router.post('/login/confirm', checkSchema({
  phone: {
    exists: {
      errorMessage: 'Не указан номер телефона'
    },
    matches: {
      options: /^77[0-9]{9}$/,
      errorMessage: 'Укажите реальный номер телефона'
    },
  },
  code: {
    exists: {
      errorMessage: 'Не указан код'
    },
    matches: {
      options: /^[0-9]{4}$/,
      errorMessage: 'Укажите верный код'
    },
  }
}), async (req, res) => {
  try {
    const code = await Code.findOne({ where: { phone: req.body.phone, code: req.body.code, used: false, createdAt: {
          [Op.gt]: moment().subtract(10, 'minutes').toDate(),
        } } })
    if (!code) {
      return res.status(422).send({
        status: 'err',
        message: 'Неверный код, либо он уже был использован'
      })
    }

    code.used = true
    await code.save()

    await User.findOne({ where: { phone: req.body.phone } }).then(async user => {
      if (!user) {
        user = await User.create({
          phone: req.body.phone
        })
      }

      let token = await Token.generate(user.id, req)

      return res.status(200).send({
        status: 'success',
        user_id: user.id,
        token: token.token,
        expires_in: token.expiresIn
      })
    })
  } catch (e) {
    console.log(e)

    return res.status(500).send({
      status: 'error',
      message: 'auth-error'
    })
  }
})

router.get('/me', authMiddleware, async (req, res) => {
  return res.status(200).send({
    status: 'ok',
    user: req.user
  })
})

router.get('/tokens', authMiddleware, async (req, res) => {
  const limit = req.query.limit || 25
  const offset = req.query.offset || 0
  const order = req.query.order || 'desc'

  const tokens = await Token.findAll({ where: { userId: req.user.id }, order: [
    ['id', order]
  ], offset: offset, limit: limit })

  return res.status(200).send({
    status: 'ok',
    tokens: tokens
  })
})

router.post('/logout', authMiddleware, async (req, res) => {
  const token = req.token

  token.deleted = true
  token.save()

  return res.status(200).send({
    status: 'ok',
    message: 'successful logout'
  })
})

router.post('/logout/:tokenId', authMiddleware, async (req, res) => {
  const tokenId = req.params.tokenId

  const token = await Token.findOne({ where: { id: tokenId, userId: req.user.id, deleted: null } })

  if(!token) return res.status(403).send({
    status: 'err',
    message: 'access denied'
  })

  token.deleted = true
  token.save()

  return res.status(200).send({
    status: 'ok',
    message: 'token deleted'
  })
})

module.exports = router
